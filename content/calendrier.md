+++
title = "Calendrier"
template = "calendar.html"
+++

  * [Calendrier des stages](/stages/calendrier.ics)
  * [Calendrier des cours](/cours/calendrier.ics)

Pour s'abonner aux calendriers (format [`ics`](https://fr.wikipedia.org/wiki/ICalendar)), copier-coller le lien dans une
application de calendrier compatible pour le garder synchronisé. Si le fichier est simplement téléchargé, il ne sera pas
maintenu à jour dans l'application.

Applications compatible, libres, et gratuites (liste non exhaustive, ce sont juste des exemples):
* Pour ordinateur (Windows, Mac, Linux): [Thunderbird](https://www.thunderbird.net/fr/)
* Pour Android: [DAVx⁵ depuis F-Droid](https://f-droid.org/packages/at.bitfire.davdroid/)
* Pour iOS: *je ne connais pas bien, de l'aide serait souhaitée pour faire des tests et fournir un exemple. Contactez moi à* **skia chez hya point sk**.