+++
title = "Yoga aux Montboucons"
description = """Hatha Yoga avec Alain Giorgetti"""
date = "2023-09-18" # date de mise à jour de l'article, pas de l'événement lui même
[taxonomies]
tags = []
[extra]
events = [
    { start = "2023-09-27T17:30:00", end = "2023-09-27T19:00:00", repeat_until = "2024-07-01T00:00:00" },
]
location = "Besançon, 35 chemin des Montboucons, Salle Émilie du Châtelet"
+++


Tarifs:
125€ + 13€ d'adhésion 2023-2024 au Comité de Quartier des Montboucons
