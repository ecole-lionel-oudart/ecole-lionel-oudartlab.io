+++
title = "KSR à Novillars"
description = """
Cours encadrés par Gwilherm Courbet
"""
date = "2023-07-05" # date de mise à jour de l'article, pas de l'événement lui même
[taxonomies]
tags = []
[extra]
events = [
    { start = "2023-09-06T20:00:00", end = "2023-09-06T22:00:00", repeat_until = "2024-06-30T00:00:00" },
]
location = "Novillars, Dojo Novillarois"
+++

----
