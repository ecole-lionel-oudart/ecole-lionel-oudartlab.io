+++
title = "Aikibugei à Grandfontaine"
description = """Aikibugei avec Julien Moreaux"""
date = "2023-08-07" # date de mise à jour de l'article, pas de l'événement lui même
[taxonomies]
tags = []
[extra]
events = [
    { start = "2023-09-05T20:30:00", end = "2023-09-05T22:00:00", repeat_until = "2024-07-01T00:00:00" },
]
location = "Dojo Kun de Grandfontaine"
+++


Tarifs:
15€/séance OU 35€/mois OU 300€/saison (accès illimité aux cours avec la formule saison).
