+++
title = "Kyudo à Grandfontaine"
description = """Kyudo avec Guillaume Jutzi - 3e dan"""
date = "2023-08-07" # date de mise à jour de l'article, pas de l'événement lui même
[taxonomies]
tags = []
[extra]
events = [
    { start = "2023-09-06T18:30:00", end = "2023-09-06T20:30:00", repeat_until = "2024-07-01T00:00:00" },
    { start = "2023-09-09T09:30:00", end = "2023-09-09T12:00:00", repeat_until = "2024-07-01T00:00:00" },
]
location = "8 impasse des Rattes 25320 Grandfontaine, Dojo Kun"
+++

Cours: A.F.C.K Association Franc-Comtoise de Kyudo <afck2590@gmail.com>  
Tarifs: AFCK 180 € + 41€ licence FFJDA Kyudo 
