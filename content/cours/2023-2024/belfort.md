+++
title = "KSR à Belfort"
description = """Katori Shinto Ryu à Belfort
Cours encadrés par Jean-Pierre Robert et Jacques Chaignon
* Lundi: Armes longues
* Vendredi: Iaijutsu
"""
date = "2023-09-04" # date de mise à jour de l'article, pas de l'événement lui même
[taxonomies]
tags = []
[extra]
events = [
    { start = "2023-09-04T20:00:00", end = "2023-09-04T22:00:00", repeat_until = "2024-07-01T00:00:00" },
    { start = "2023-09-08T20:00:00", end = "2023-09-08T22:00:00", repeat_until = "2024-07-01T00:00:00" },
]
location = "Belfort, gymnase Pierre de Coubertin"
+++

----
