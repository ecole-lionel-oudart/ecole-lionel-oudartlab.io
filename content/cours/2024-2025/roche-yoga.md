+++
title = "Yoga à Roche lez Beaupré"
description = """Hatha Yoga avec Alain Giorgetti et Dominique Bernard"""
date = "2024-09-18" # date de mise à jour de l'article, pas de l'événement lui même
[taxonomies]
tags = []
[extra]
events = [
    { start = "2024-09-05T20:00:00", end = "2024-09-05T21:30:00", repeat_until = "2025-07-01T00:00:00" },
]
location = "Roche-lez-Beaupré, Salle du SIVU"
+++


Tarifs:
70€ à l'association Vivre Autrement

