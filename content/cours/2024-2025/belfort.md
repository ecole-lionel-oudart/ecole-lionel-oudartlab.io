+++
title = "KSR à Belfort"
description = """Katori Shinto Ryu à Belfort
Cours encadrés par Jean-Pierre Robert et Jacques Chaignon
* Lundi: Armes longues
* Vendredi: Iaijutsu
"""
date = "2024-09-18" # date de mise à jour de l'article, pas de l'événement lui même
[taxonomies]
tags = []
[extra]
events = [
    { start = "2024-09-02T20:00:00", end = "2024-09-02T22:00:00", repeat_until = "2025-07-01T00:00:00" },
    { start = "2024-09-06T20:00:00", end = "2024-09-06T22:00:00", repeat_until = "2025-07-01T00:00:00" },
]
location = "Belfort, gymnase Pierre de Coubertin"
+++

----
