+++
title = "Bujutsu à Novillars"
description = """
Cours encadrés par Gwilherm Courbet
"""
date = "2024-09-18" # date de mise à jour de l'article, pas de l'événement lui même
[taxonomies]
tags = []
[extra]
events = [
    { start = "2024-09-04T18:30:00", end = "2024-09-04T20:00:00", repeat_until = "2024-10-28T00:00:00" },
]
location = "Roche-lez-Beaupré, Salle du SIVU"
+++

----
