+++
title = "Yoga aux Montboucons"
description = """Hatha Yoga avec Alain Giorgetti"""
date = "2024-09-18" # date de mise à jour de l'article, pas de l'événement lui même
[taxonomies]
tags = []
[extra]
events = [
    { start = "2024-09-18T17:30:00", end = "2024-09-18T19:00:00", repeat_until = "2025-07-01T00:00:00" },
]
location = "Besançon, 35 chemin des Montboucons, Salle Émilie du Châtelet"
+++


Tarifs:
125€ + 13€ d'adhésion 2024-2025 au Comité de Quartier des Montboucons
