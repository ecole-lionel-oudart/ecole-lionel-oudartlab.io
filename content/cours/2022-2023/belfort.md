+++
title = "KSR à Belfort"
description = """Katori Shinto Ryu à Belfort
Cours encadrés par Jean-Pierre et Jacques
* Lundi: Armes longues
* Vendredi: Iaijutsu
"""
date = "2022-09-05" # date de mise à jour de l'article, pas de l'événement lui même
[taxonomies]
tags = []
[extra]
events = [
    { start = "2022-09-05T20:00:00", end = "2022-09-05T22:00:00", repeat_until = "2023-07-01T00:00:00" },
    { start = "2022-09-09T20:00:00", end = "2022-09-09T22:00:00", repeat_until = "2023-07-01T00:00:00" },
]
location = "Belfort, gymnase Pierre de Coubertin"
+++

# Cours hebdomadaire

* Lundi: Armes longues
* Vendredi: Iaijutsu

