+++
title = "KSR à Novillars"
description = """Katori Shinto Ryu à Novillars
Cours encadrés par Gwilherm
"""
date = "2022-09-07" # date de mise à jour de l'article, pas de l'événement lui même
[taxonomies]
tags = []
[extra]
events = [
    { start = "2022-09-07T20:00:00", end = "2022-09-07T22:00:00", repeat_until = "2023-07-01T00:00:00" },
]
location = "Novillars, Dojo Novillarois"
+++

# Cours hebdomadaire

Encadrant: Gwilherm Courbet
