+++
title = "ANNULÉ - Kobujutsu"
description = "ANNULÉ - Stage d'Octobre en Kobujutsu avec Julien Moreaux"
date = "2023-09-28" # date de mise à jour de l'article, pas de l'événement lui même
[taxonomies]
tags = []
[extra]
events = [
    { start = "2023-10-14T15:00:00", end = "2023-10-14T19:00:00" },
    { start = "2023-10-15T09:00:00", end = "2023-10-15T12:00:00" },
    { start = "2023-10-15T14:00:00", end = "2023-10-15T17:00:00" },
]
location = "Dojo Novillarois, Novillars"
+++

Tarifs = 50€/journée OU 80€/weekend.

**Important**: le stage est annulé
