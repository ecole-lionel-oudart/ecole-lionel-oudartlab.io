+++
title = "Hatha Yoga"
description = "Stage intensif de Hatha Yoga avec Julien Moreaux à Novillars"
date = "2023-11-04" # date de mise à jour de l'article, pas de l'événement lui même
[taxonomies]
tags = []
[extra]
events = [
    { start = "2023-11-18T15:00:00", end = "2023-11-18T19:00:00" },
    { start = "2023-11-19T09:00:00", end = "2023-11-19T12:00:00" },
    { start = "2023-11-19T14:00:00", end = "2023-11-19T17:00:00" },
]
location = "Dojo Novillarois, Novillars"
+++

Les inscriptions pour le prochain stage de HATHA YOGA à Novillars sont officiellement ouvertes !

  * Samedi 18 novembre de 15h à 19h
  * Dimanche 19 novembre 9h-12h et 14h-17h

L’objectif de ce stage est de rassembler et de pratiquer tous ensemble. Il s’agit d’un stage intensif sur 2 jours ouvert
aussi bien aux débutants qu’aux anciens.

La particularité de ce stage réside dans son nouveau mode de tarification. Ainsi nous parlerons de « valeur financière »
et non plus de tarifs. Cette valeur financière est fixée à 50€/demie journée OU 80€/weekend.

Cela signifie que chacun donne ce qu’il veut/peut tout en ayant connaissance de la valeur financière du stage. Si vous
souhaitez assister au stage et ne rien donner du tout vous pouvez ! Si vous souhaitez donner plus ou moins que la valeur
financière vous le pouvez aussi ! C’est vous qui décidez…Ainsi dans cette période d’augmentation du coût de la vie,
je tiens à ce que l’argent ne sera plus un facteur limitant à votre pratique en stage. Vous pourrez vous-même estimer
la valeur de ce que le stage vous aura apporter. La participation financière, si elle a lieu, se fera dans une urne de
manière anonyme.

Si ce stage vous intéresse ne tardez pas à vous inscrire sur le lien doodle car les places sont limitées :
<https://framadate.org/ccJezfZJOxYMlNRO>
