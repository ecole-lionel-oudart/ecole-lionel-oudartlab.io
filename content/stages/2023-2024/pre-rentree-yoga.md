+++
title = "Hatha Yoga"
description = "Stage de pré-rentrée en Hatha Yoga avec Julien Moreaux"
date = "2023-08-31" # date de mise à jour de l'article, pas de l'événement lui même
[taxonomies]
tags = []
[extra]
events = [
    { start = "2023-09-02T07:00:00", end = "2023-09-02T10:00:00" },
]
location = "28 chemin de la Chassagne, 25720 FONTAIN"
+++


Le stage de Pré-Rentrée en Hatha Yoga aura lieu le Samedi 2 Septembre de 7h à 10h en extérieur aux [Bassins Fermiers](https://lesbassinsfermiers.fr/) à Fontain.

Adresse : 28 chemin de la Chassagne, 25720 FONTAIN.

Au programme :
  * 7h Début du stage
  * Mondo (introduction)
  * Exposition volontaire au froid de bon matin 😊 avec explications et accompagnement.
  * Kriya Yoga (exercices de purifications internes)
  * Yoga chinois
  * Asanas
  * Yoga nidra
  * 10h fin du stage, café et thé seront servie sur place à l’intérieur.
  * 10h30 visite de la ferme (aquaculture de spiruline) commentée par le gérant Vivien Desgranges.

Tarifs = 20€/personne à régler sur place (chèques, espèce et CB seront acceptés).

Prévoir des vêtements amples, tapis de yoga ou couverture ainsi que vêtement chaud au besoin.

Si vous souhaitez vous inscrire, faites le moi savoir par retour de mail ou par téléphone (0688084055).

Très bon été à vous !

Julien MOREAUX