+++
title = "Aiki + Kobujutsu à Novillars"
description = "Stage de Kobujutsu le Samedi, et d'Aiki le Dimanche avec Julien au Dojo de Novillars"
date = "2023-12-06" # date de mise à jour de l'article, pas de l'événement lui même
[taxonomies]
tags = []
[extra]
events = [
    { start = "2023-12-09T15:00:00", end = "2023-12-09T19:00:00" },
    { start = "2023-12-10T09:00:00", end = "2023-12-10T12:00:00" },
    { start = "2023-12-10T14:00:00", end = "2023-12-10T17:00:00" },
]
location = "Dojo Novillarois, Novillars"
+++

Vous trouverez ci-joint les informations concernant le prochain stage d’AIKI à Novillars.

Ce stage s’adresse aussi bien aux débutants qu’aux avancés et sera l’occasion d’apprendre à stopper la violence sans y participer pour gagner en confiance en soi.

* Samedi 9 décembre : 15h-19h - Kobujutsu
* Dimanche 10 décembre : 9h-12h & 14h-17h - Aiki

Comme pour le stage de Yoga, le tarif sera libre et anonyme.

Inscriptions sur le lien suivant => <https://framadate.org/oy41qtLPwxgHkZ6I>

 
