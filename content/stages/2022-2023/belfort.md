+++
title = "Stage à Belfort"
description = "Un stage avec le Gakko Dento"
date = "2022-11-12" # date de mise à jour de l'article, pas de l'événement lui même
[taxonomies]
tags = []
[extra]
events = [
    { start = "2022-11-12T15:00:00", end = "2022-11-12T19:00:00" },
    { start = "2022-11-13T09:00:00", end = "2022-11-13T17:00:00" },
]
location = "Belfort, gymnase Leo Lagrange"
+++

# Stage à Belfort

Remise de grade premier degré
