+++
title = "Stage d'été à Novillars"
description = "Stage d'été multi-disciplines avec Novil'O Budo"
date = "2023-07-10" # date de mise à jour de l'article, pas de l'événement lui même
[taxonomies]
tags = []
[extra]
events = [
    { start = "2023-07-10T18:00:00", end = "2023-07-10T22:00:00" },
    { start = "2023-07-11T18:00:00", end = "2023-07-11T22:00:00" },
    { start = "2023-07-12T18:00:00", end = "2023-07-12T22:00:00" },
    { start = "2023-07-13T18:00:00", end = "2023-07-13T22:00:00" },
    { start = "2023-07-14T14:00:00", end = "2023-07-14T18:00:00" },
]
location = "Novillars, Dojo Novillarois"
+++

# Stage multi-disciplines à Novillars

* Yoga
* Kyudo
* Katori
* Aiki
* Kobu