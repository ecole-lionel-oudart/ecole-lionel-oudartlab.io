+++
title = "Stage à Novillars"
description = "Un stage avec Novil'O Budo"
date = "2023-03-18" # date de mise à jour de l'article, pas de l'événement lui même
[taxonomies]
tags = []
[extra]
events = [
    { start = "2023-03-18T15:00:00", end = "2023-03-18T19:00:00" },
    { start = "2023-03-19T09:00:00", end = "2023-03-19T17:00:00" },
]
location = "Novillars, Dojo Novillarois"
+++

# Stage à Novillars

Remise de grade deuxième degré