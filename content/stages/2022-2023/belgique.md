+++
title = "Stage en Belgique"
description = "Un stage avec Mushin No Ryu"
date = "2023-06-24" # date de mise à jour de l'article, pas de l'événement lui même
[taxonomies]
tags = []
[extra]
events = [
    { start = "2023-06-24T15:00:00", end = "2023-06-24T19:00:00" },
    { start = "2023-06-25T09:00:00", end = "2023-06-25T17:00:00" },
]
location = "Louvain la Neuve, complexe sportif de Blocry"
+++

# Stage à Louvain la Neuve

Un petit stage de printemps au pays de la bière.


