#!/bin/sh

BASE_DIR="$(dirname "$0")"

cd "$BASE_DIR"

zola build

find public -name '*.ics' | while read -r file
do
    echo "fixing $file"
    mv "$file/index.html" "${file}2"
    rmdir "$file"
    mv "${file}2" "$file"
done
