# About this code

This code is an aggregation of many old libraries hacked together to make a correct calendar experience.

## Minifying

https://minify-js.com/

Those files have been manually minified for a lighter online experience, but the full source remained if code modifications are needed:

* `fullcalendar.js`
* `ical.js`
