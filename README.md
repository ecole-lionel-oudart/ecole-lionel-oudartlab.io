# École Lionel Oudart

Ce site a pour but de fournir un calendrier à jour et synchronisable des stages et cours de l'école Lionel Oudart.

Contacter les développeurs:
  * skia chez hya point sk
  * jc_loudart chez hystoires point com

# A website that uses [Zola](https://www.getzola.org/)

## Development

```
./serve.sh
```

## Generating the static site

```
./build.sh
```

## Including a gallery in a post

* Use [YOGA](https://yoga.flozz.org/) to reduce the size of the pictures (JPEG 60%, 1600px max, is far enough for the
  web!), and store the optimized images in `path/to/opti`.
* Use `fgallery -s path/to/opti ./content/.../my-post/gallery`.
* Replace the processed images as `YOGA` does a better job than `fgallery`:  
  `rm my-post/gallery/imgs/* && cp path/to/opti/* my-post/gallery/imgs/`
* Link it with `[link to gallery](./gallery/)` from the post in Markdown.
