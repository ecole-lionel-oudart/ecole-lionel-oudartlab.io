#!/bin/bash

find . -not -path "./.git/*"  -not -path "./public/*" | entr -rs "zola build -u http://127.0.0.1:8000; cd public; python3 -m http.server"
